bin/basic: src/y.tab.c src/lex.yy.c
	cd src && \
	cc lex.yy.c y.tab.c -o ../bin/basic
src/y.tab.c: src/basic.y
	cd src && \
	yacc -d basic.y -v
src/lex.yy.c: src/basic.l
	cd src && \
	lex basic.l
clean:
	rm -f src/y.tab.c src/y.tab.h src/y.output src/lex.yy.c bin/basic
