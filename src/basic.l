%{
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
void yyerror(char *);
#include "y.tab.h"
%}
%%
['][^\n]*				;
[Rr][Ee][Mm][ \t]+[^\n]*		{return REM;}
[Pp][Rr][Ii][Nn][Tt]			{return PRINT;}
[Ll][Ee][Tt]				{return LET;}
[Aa][Nn][Dd]				{return AND;}
[Oo][Rr]				{return OR;}
[Nn][Oo][Tt]				{return NOT;}
"<>"					{return NE;}
"<="					{return LE;}
">="					{return GE;}
"<"					{return LT;}
">"					{return GT;}
[Ii][Ff]				{return IF;}
[Tt][Hh][Ee][Nn]			{return THEN;}
[Ee][Ll][Ss][Ee]			{return ELSE;}
[Ee][Nn][Dd][ \t]+[Ii][Ff]		{return ENDIF;}
[Ww][Hh][Ii][Ll][Ee]			{return WHILE;}
[Ww][Ee][Nn][Dd]			{return WEND;}
[Dd][Oo]				{return DO;}
[Ll][Oo][Oo][Pp]			{return LOOP;}
[Uu][Nn][Tt][Ii][Ll]			{return UNTIL;}
[Gg][Oo][Tt][Oo]			{return GOTO;}
[Ee][Nn][Dd]				{return END;}
[A-Za-z][A-Za-z0-9]*[:] 		{yylval.strval=strdup(yytext); return LABEL;}
[0-9]+(\.[0-9]+)?([Ee][+-]?[0-9]+)?	{yylval.strval=strdup(yytext); return NUMBER;}
\"[^\"\n]*\"				{yylval.strval=strdup(yytext); return STR;}
[A-Za-z][A-Za-z0-9]* 			{yylval.strval=strdup(yytext); return IDENTIFIER;}
[-+()=/*;,\n:\^] 			{return *yytext;}
[ \t]					;
.					yyerror("invalid character");
%%
int yywrap(void) {
return 1;
}
