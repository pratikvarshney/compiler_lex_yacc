%name parse
%{
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
%}
%union {
int intval;
char* strval;
}

%start line

%token<strval> NUMBER
%token<strval> NEGATION
%token<strval> IDENTIFIER
%token<strval> LABEL
%token<strval> STR

%token PRINT LET END REM OR AND NOT IF THEN ELSE ENDIF WHILE WEND DO LOOP UNTIL GOTO
%token LT GT NE LE GE EQ

%nonassoc IFX
%nonassoc ELSE

%right '='

%left OR
%left AND
%left NOT

%left LT GT NE LE GE EQ

%left '+' '-'
%left '*' '/'
%left NEGATION UPLUS
%left '^'

%right ',' ';'

%type<strval> expr
%type<strval> if_expr
%type<strval> if_then
%type<strval> if_then_gt
%type<strval> while_condition
%type<strval> while_begin
%type<strval> until_condition
%type<strval> until_begin
%type<strval> do_stmt
%type<intval> while_until

%{
	void yyerror(char *);
	int yylex(void);
	char* getNewTemp();
	char* getNewLabel();
	FILE *outp;
	int tempIndex=0;
	char tempVar[12];
	char tempLabel[12];
	int tempLabelIndex=0;
	int newline=1;
	char *tempStr, *tempStr2;
%}
%%
line:
	line command seperator
	| line REM '\n'
	|
	;
seperator:
	'\n'
	| ':'
	;
command:
	PRINT plist				{if(newline==1){ fprintf(outp,"PRINT NEWLINE\n");} newline=1;}
	| LET IDENTIFIER '=' expr		{fprintf(outp,"%s = %s\n", $2, $4);}
	| IDENTIFIER '=' expr 			{fprintf(outp,"%s = %s\n", $1, $3);}
	| LABEL					{fprintf(outp,"%s\n", $1);}
	| GOTO IDENTIFIER			{fprintf(outp,"GOTO %s\n", $2);}
	| if_stmt ENDIF	
	| while_stmt WEND
	| DO until_while_stmt LOOP
	| do_stmt line LOOP while_until expr	{if($4==0){tempStr=getNewTemp(); fprintf(outp,"%s = NOT %s\n", tempStr, $5);} else {tempStr=$5;} fprintf(outp,"IF %s != 0 GOTO %s\n", tempStr, $1);}	
	| END					{fprintf(outp,"EXIT\n"); return 0;}
	|
	;
until_while_stmt:
	until_stmt
	| while_stmt
	;
while_until:
	UNTIL					{$$=0;}
	| WHILE					{$$=1;}
	;
do_stmt:
	DO seperator				{$$=getNewLabel(); fprintf(outp,"%s:\n", $$);}
	;
until_stmt:
	until_begin until_condition line	{fprintf(outp,"GOTO %s\n%s:\n", $1, $2);}
	;
until_condition:
	expr					{
							tempStr2=getNewTemp();
							fprintf(outp,"%s = NOT %s\n", tempStr2, $1);
							$$=getNewLabel();
							tempStr=getNewLabel();
							fprintf(outp,"IF %s != 0 GOTO %s\nGOTO %s\n%s:\n", tempStr2, tempStr, $$, tempStr);
						}
	;
until_begin:
	UNTIL					{$$ = getNewLabel(); fprintf(outp,"%s:\n", $$);}
	;
while_stmt:
	while_begin while_condition line	{fprintf(outp,"GOTO %s\n%s:\n", $1, $2);}
	;
while_condition:
	expr					{$$=getNewLabel(); tempStr=getNewLabel(); fprintf(outp,"IF %s != 0 GOTO %s\nGOTO %s\n%s:\n", $1, tempStr, $$, tempStr);}
	;
while_begin:
	WHILE					{$$ = getNewLabel(); fprintf(outp,"%s:\n", $$);}
	;
if_stmt:
	if_then %prec IFX			{fprintf(outp,"%s:\n", $1);}
	| if_then_gt ELSE line			{fprintf(outp,"%s:\n", $1);}
	;
if_then_gt:
	if_then					{$$ = getNewLabel(); fprintf(outp,"GOTO %s\n%s:\n", $$, $1);}
	;	
if_then:
	if_expr THEN line			{$$=$1;}
	;
if_expr:
	IF expr					{tempStr = getNewLabel(); $$ = getNewLabel(); fprintf(outp,"IF %s != 0 GOTO %s\nGOTO %s\n%s:\n", $2, tempStr, $$, tempStr);}
	;
plist: 
	ptab ',' plist
	| pspace ';' plist
	| pcomm
	;
ptab:
	pcomm					{fprintf(outp,"PRINT TAB\n");newline=0;}
	;
pspace:
	pcomm					{fprintf(outp,"PRINT SPACE\n");newline=0;}
	;
pcomm:
	expr					{fprintf(outp,"PRINT %s\n", $1);newline=1;}
	| STR					{fprintf(outp,"PRINT %s\n", $1);newline=1;}
	|
	;
expr:
	NUMBER					{$$ = $1;}
	| IDENTIFIER				{$$ = $1;}
	| '(' expr ')'				{$$ = $2;}
	/* ARITHMATIC EXPRESSION */
	| expr '+' expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s + %s\n",$$, $1, $3);}
	| expr '-' expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s - %s\n",$$, $1, $3);}
	| expr '*' expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s * %s\n",$$, $1, $3);}
	| expr '/' expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s / %s\n",$$, $1, $3);}
	| expr '^' expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s ^ %s\n",$$, $1, $3);}
	| '-' expr	%prec NEGATION		{$$ = getNewTemp(); fprintf(outp, "%s = -%s\n",$$, $2);}
	| '+' expr	%prec UPLUS		{$$ = $2;}
	/* LOGICAL EXPRESSION */
	| expr AND expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s AND %s\n",$$, $1, $3);}
	| expr OR expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s OR %s\n",$$, $1, $3);}
	| NOT expr				{$$ = getNewTemp(); fprintf(outp, "%s = NOT %s\n",$$, $2);}
	/* RELATIONAL EXPRESSION */
	| expr LT expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s < %s\n",$$, $1, $3);}
	| expr GT expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s > %s\n",$$, $1, $3);}
	| expr NE expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s != %s\n",$$, $1, $3);}
	| expr LE expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s <= %s\n",$$, $1, $3);}
	| expr GE expr				{$$ = getNewTemp(); fprintf(outp, "%s = %s >= %s\n",$$, $1, $3);}
	| expr '=' expr	%prec EQ		{$$ = getNewTemp(); fprintf(outp, "%s = %s == %s\n",$$, $1, $3);}
	;
%%
void yyerror(char *s) {
	fprintf(stderr, "%s\n", s);
}
char* getNewTemp(){
	tempIndex++;
	sprintf(tempVar,"t%d", tempIndex);
	return strdup(tempVar);
}
char* getNewLabel(){
	tempLabelIndex++;
	sprintf(tempLabel,"L%d", tempLabelIndex);
	return strdup(tempLabel);
}
int main(void) {
	outp=stdout;
	yyparse();
	return 0;
}

